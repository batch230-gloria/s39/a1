const mongoose = require("mongoose");
const Course = require("../models/course.js");

// Function foir adding a course
// 2. Update the "addCourse" controller method to implement admin authentication for creating a course
// NOTE: [1] include screenshot of successful admin addCourse and [2] screenshot of not successful add course by a user that is not admin
/*module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({
		name:reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((newCourse, error) =>
		{
			if(error){
				return error;
			}
			else{
				return newCourse;
			}
		}
	)
}*/

[ACTIVITY - s39]
module.exports.addCourse = (reqBody,admin) => {
	if(admin.isAdmin == true){

		let newCourse = new Course({
			name:reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})

		return newCourse.save().then((newCourse, error) =>
			{
				if(error){
					return error;
				}
				else{
					return newCourse;
				}
			}
		)
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}

}


// GET all course
module.exports.getAllCourse = () =>{
	return Course.find({}).then(result =>{
		return result;
	})
}

// GET all Active Courses
module.exports.getActiveCourse = () =>{
	return Course.find({isActive:true}).then(result =>{
		return result;
	})
}

// GET specific course
module.exports.getCourse = (courseId) =>{
	return Course.findById(courseId).then(result =>{
		return result;
	})
}


// UPDATING a course
/*module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				// newData.course.name
				// newData.request.body.name
				name: newData.course.name, 
				description: newData.course.description,
				price: newData.course.price
			}
		).then((result, error)=>{
			if(error){
				return false
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}*/


