const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers.js");
const auth = require("../auth.js")

// Activity
// 1. Update the "course" route to implement user authentication for the admin when creating a course.


// Creating a course
/*router.post("/create", (request, response) => {
	courseControllers.addCourse(request.body).then(resultFromController => response.send(resultFromController))
})*/

[ACTIVITY - s39]
router.post("/create", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body, 
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseControllers.addCourse(request.body, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})


// Get all courses 
router.get("/all", (request, response) => {
	courseControllers.getAllCourse().then(resultFromController => response.send(resultFromController))
})

// Get all ACTIVE courses 
router.get("/active", (request, response) => {
	courseControllers.getActiveCourse().then(resultFromController => response.send(resultFromController))
})

// GET specific course
router.get("/:courseId", (request, response) => {
	courseControllers.getCourse(request.params.courseId).then(resultFromController => response.send(resultFromController))
})


/*router.patch("/:courseId/update", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseControllers.updateCourse(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})*/


module.exports = router;

